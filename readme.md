## Curso Laravel com Docker

-   Docker Image: Optimized: docker push jairdeon/laravel:optimized

## Passos para instalação:

-   git clone https://jairdeon@bitbucket.org/jairdeon/code-education.git .
-   docker-compose up -d
-   Por gentileza, aguardar a instalação de todos os processos. Acompanhe utilizando o "docker logs app" até que a última linha seja "Generating optimized autoload files"
